require 'msip/concerns/controllers/hogar_controller'

module Msip
  class HogarController < ApplicationController

    include Msip::Concerns::Controllers::HogarController

    def ppd
      render layout: 'application'
    end


    def gracias
      render layout: 'application'
    end

  end
end
