
require 'msip/concerns/models/municipio'

module Msip
  class Municipio < ActiveRecord::Base
    include Msip::Concerns::Models::Municipio

    scope :filtro_permanente, -> () {
      joins(:departamento).where('msip_departamento.id_pais' => 170) 
    }

  end
end
