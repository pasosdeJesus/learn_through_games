class Ability  < Cor1440Gen::Ability

  ROLADMIN = 1
  ROLOPERADOR = 5
  ROLES = [
    ["Administrador", ROLADMIN], #1
    ["", 0], #2
    ["", 0], #3
    ["", 0], #4
    ["Sistematizador", ROLOPERADOR], #5
    ["", 0], #6
  ]

  ROLES_CA = [
    "Administrar cualquier dato", #1
    "", #2
    "", #3
    "", #4
    "Ver Elecciones. "+
    "Ver Formularios E-14 existentes, crear nuevos y editar los que cree.", #5
    "", #6
    "" #7
  ]


  # Autorizacion con CanCanCan
  def initialize(usuario = nil)
    initialize_cor1440_gen(usuario)

    # Usuarios anonimos
    #can :contar, Sivel2Gen::Caso
    if usuario && usuario.rol then
      can :read, ::Usuario
      case usuario.rol
      when Ability::ROLOPERADOR
      when Ability::ROLADMIN
      end
    end
  end

end
