class ApplicationRecord < ActiveRecord::Base
  include Msip::Modelo
  include Msip::Localizacion
  #include Msip::FormatoFecha

  self.abstract_class = true
end
