
Puede ver el listado de personas que han implementado en:
<https://gitlab.com/pasosdeJesus/learntg/-/graphs/main>

Imagen de portada basada en:
<https://www.publicdomainpictures.net/en/free-download.php?image=baby-with-play-balls&id=26005>

Clip de anexos: <https://openclipart.org/detail/168413/paper-clip>

Base para la foto de usuario predeterminada:
<https://openclipart.org/image/400px/15812 >

Esta aplicación usa los motores 
[msip](https://github.com/pasosdeJesus/msip),
[mr519_gen](https://github.com/pasosdeJesus/mr519_gen) y
[heb412_gen](https://github.com/pasosdeJesus/heb412_gen)
[cor1440_gen](https://github.com/pasosdeJesus/cor1440_gen)
también mantenidos y re-factorizados por Pasos de Jesús de 
aplicaciones web desarrolladas voluntariamente o financiadas por 
diversas organizaciones que han cedido al dominio público lo desarrollado, 
ver por ejemplo
<https://github.com/pasosdeJesus/sivel2/blob/master/CREDITOS.md>

También usa una pila tecnológica de código abierto que incluye 
PostgreSQL, nginx, Ruby, nodejs y Ruby on Rails, así como muchas
gemas ruby y paquetes npm.

El sistema operativo de desarrollo y objetivo principal para producción
ha sido adJ/OpenBSD, ver https://aprendiendo.pasosdeJesus.org


Agradecemos a Jesús por enseñarnos y enviarnos a enseñar:

  Jesús fue donde ellos y les habló diciendo, 
    «Se me ha dado toda autoridad en el cielo y sobre la tierra.
     Entonces vayan y hagan discípulos en todas las naciones, bautizándolos 
     en el nombre del Padre, y del Hijo y del Espíritu Santo, 
     enseñándoles a seguir todas las cosas que les he ordenado.
     Miren, estoy con ustedes siempre, hasta el fin del mundo» 
  Amén

  Mateo 28:18-20

