# Learn Through Games - LMS

[![Revisado por Hound](https://img.shields.io/badge/Reviewed_by-Hound-8E64B0.svg)](https://houndci.com) Pruebas y seguridad:[![Estado Construcción](https://gitlab.com/pasosdeJesus/learn_through_games/badges/main/pipeline.svg)](https://gitlab.com/pasosdeJesus/learn_through_games/-/pipelines?page=1&scope=all&ref=main) [![Clima del Código](https://codeclimate.com/github/pasosdeJesus/learn_through_games/badges/gpa.svg)](https://codeclimate.com/github/pasosdeJesus/learn_through_games) [![Cobertura de Pruebas](https://codeclimate.com/github/pasosdeJesus/learn_through_games/badges/coverage.svg)](https://codeclimate.com/github/pasosdeJesus/learn_through_games)

Plataforma de aprendizaje

### Requerimientos
* Ruby version >= 3.1
* PostgreSQL >= 14.0 con extensión unaccent disponible
* Recomendado sobre adJ 7.1 (que incluye todos los componentes mencionados).
  Las instrucciones que siguen suponen que opera en este ambiente.


### Arquitectura

Es una aplicación que emplea los siguientes motores:
* Motor genérico estilo Pasos de Jesús `msip`, ver <https://gitlab.com/pasosdeJesus/msip>
* Formularios y encuestas `mr519_gen`, ver
  <https://gitlab.com/pasosdeJesus/mr519_gen>
* Nube de documentos y plantillas en hojas de cálculo `heb412_gen`, ver
  <https://gitlab.com/pasosdeJesus/heb412_gen>
* Proyectos y actividades con metodología de marco lógico `cor1440_gen`, ver
  <https://github.com/pasosdeJesus/cor1440_gen>

## Uso

Por favor vea las instrucciones de sivel2 pues es muy similar:
https://github.com/pasosdeJesus/sivel2

